#ifndef EFFECT_H
#define EFFECT_H

#include <QString>

class Effect
{
public:
    enum class Type
    {
        Red,
        Green,
        Blue,
        Black
    };

public:
    Effect(Type aType);

    Type type() const;
    void setType( const Type aType);
    QString typeDescription() const;

private:
    Type m_type;
};

#endif // EFFECT_H
