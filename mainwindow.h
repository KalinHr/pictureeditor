#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "tablemodel.h"
#include "effectsmodel.h"
#include "picture.h"
#include "effect.h"

#include <QTableView>
#include <QProgressBar>
#include <QCheckBox>
#include <QPushButton>
#include <QComboBox>
#include <QLabel>
#include <QString>
#include <QFutureWatcher>
#include <QMainWindow>
#include <QActionGroup>
#include <QTranslator>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = Q_NULLPTR);

protected:
    void changeEvent( QEvent* ) Q_DECL_OVERRIDE;

private:
    static const QSet< QString > s_supportedImageFormats;

private slots:
    void processImageConversionFinished();
    void effectChanged(int aEffect);
    void startClicked();
    void addImageClicked();
    void removeImageClicked();
    void removeAllImagesClicked();
    void progressBarValueChanged();

    void english();
    void bulgarian();

private:
    static bool isValidDragAndDropEvent( QDropEvent* const aDragAndDropEvent );

private:
    TableModel   *m_tableModel;
    EffectsModel *m_effectModel;

    QTableView   *m_table;
    QProgressBar *m_progressBar;
    QCheckBox    *m_saveInFolder;
    QPushButton  *m_addImage;
    QPushButton  *m_removeImage;
    QPushButton  *m_removeAllImages;
    QComboBox    *m_effects;
    Effect        m_effect;
    QLabel       *m_before;
    QLabel       *m_after;
    QPushButton  *m_start;

    QMenu        *m_menu;

    QActionGroup *m_languageGroup;
    QAction      *m_english;
    QAction      *m_bulgarian;

    QTranslator   m_translator;

    QHash< QFutureWatcher< void >*, Picture* > m_imageConverters;
    int m_imageConvertersSize;

    static void useEffect(Picture *aPicture, Effect aEffect, QString aDirectory);

    void buttonStyle(QPushButton *aButton, QString aPath);

    void dragEnterEvent(QDragEnterEvent *event) Q_DECL_OVERRIDE;
    void dropEvent(QDropEvent *event) Q_DECL_OVERRIDE;

    void createActions();
    void createMenus();
};

#endif // MAINWINDOW_H
