#ifndef PICTURE_H
#define PICTURE_H

#include <QIcon>
#include <QString>
#include <QVariant>

class Picture
{
public:
    enum class Status
    {
        Pending,
        InProgress,
        Ready
    };

public:
    Picture(const QString& aPath);

    QString path() const;
    const QIcon& image() const;

    void setStatus( const Status aStatus);
    Status status() const;
    QString statusDescription() const;
    QIcon statusImage() const;

private:
    QString m_path;
    QIcon m_pixmap;
    Status m_status;
};

#endif // PICTURE_H
