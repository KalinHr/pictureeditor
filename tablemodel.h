#ifndef TABLEMODEL_H
#define TABLEMODEL_H

#include "picture.h"

#include <QAbstractTableModel>
#include <QString>

class TableModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit TableModel( QObject *parent = Q_NULLPTR );

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation = Qt::Horizontal,
                        int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

    void addPicture( Picture* aPicture );
    void removeSelectedRows(QModelIndexList rows);
    void removeAllRows();

    void dataUpdated();

    QList< Picture* > pictures();

private:
    enum ColumnIndex
    {
        ColumnIndexImage = 0,
        ColumnIndexStatus = 1,
        ColumnsCount = 2
    };

private:
    QList< Picture* > m_pictures;
};

#endif // TABLEMODEL_H
