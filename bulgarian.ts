<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="bg_BG">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="52"/>
        <source>Save in folder</source>
        <translation>Запиши в папка</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="74"/>
        <location filename="mainwindow.cpp" line="346"/>
        <source>Start</source>
        <translation>Старт</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="124"/>
        <source>Select Picture</source>
        <translation>Избери картинка</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="155"/>
        <source>Open Directory</source>
        <translation>Отвори Папка</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="296"/>
        <location filename="mainwindow.cpp" line="344"/>
        <source>English</source>
        <translation>Английски</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="299"/>
        <location filename="mainwindow.cpp" line="345"/>
        <source>Bulgarian</source>
        <translation>Български</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="313"/>
        <source>Language</source>
        <translation>Език</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="effect.cpp" line="22"/>
        <source>red</source>
        <translation>червено</translation>
    </message>
    <message>
        <location filename="effect.cpp" line="23"/>
        <source>green</source>
        <translation>зелено</translation>
    </message>
    <message>
        <location filename="effect.cpp" line="24"/>
        <source>blue</source>
        <translation>синьо</translation>
    </message>
    <message>
        <location filename="effect.cpp" line="25"/>
        <source>black</source>
        <translation>черно</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="220"/>
        <source>%1/%2_new.jpg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="picture.cpp" line="33"/>
        <source>Pending</source>
        <translation>Не Стартиран</translation>
    </message>
    <message>
        <location filename="picture.cpp" line="34"/>
        <source>In Progress ...</source>
        <translation>В Прогрес ...</translation>
    </message>
    <message>
        <location filename="picture.cpp" line="35"/>
        <source>Ready</source>
        <translation>Готово</translation>
    </message>
</context>
<context>
    <name>TableModel</name>
    <message>
        <location filename="tablemodel.cpp" line="77"/>
        <source>Image</source>
        <translation>Картинка</translation>
    </message>
    <message>
        <location filename="tablemodel.cpp" line="79"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
</context>
</TS>
